def field(game):
    column = ""
    lines = len(game.field)
    columns = len(game.field[0])
    for y in range(lines):
        for x in range(columns):
            if game.field[y][x]:
                column += "█"
            else:
                column += " "
        column += "\n"
    print(column)
