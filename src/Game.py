import numpy as numpy


class Game:

    def __init__(self, lines, columns):
        self.field = [[False] * lines] * columns

    def init_field(self):
        lines = len(self.field[0])
        columns = len(self.field)

        self.field = numpy.random.choice([True, False], size=(lines, columns))

    def set_point(self, x, y, value):
        self.field[y][x] = value
