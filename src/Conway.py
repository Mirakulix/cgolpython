from src.Game import Game


def game_of_life(current_generation: Game):
    lines = len(current_generation.field)
    columns = len(current_generation.field[0])

    next_generation = Game(lines, columns)
    next_generation.init_field()
    for x in range(columns):
        for y in range(lines):
            next_generation.set_point(x, y, _apply_rules(current_generation, x, y))

    return next_generation


def _apply_rules(game, x, y) -> bool:
    neighbours = _count_neighbours(game.field, x, y)

    if game.field[y][x]:  # case survive
        if neighbours == 2 or neighbours == 3:
            return True
    else:  # case born
        if neighbours == 3:
            return True
    return False


def _count_neighbours(field, x, y) -> int:
    lines = len(field)
    columns = len(field[0])

    y_top = (y - 1) % lines
    y_bot = (y + 1) % lines
    x_left = (x - 1) % columns
    x_right = (x + 1) % columns

    neighbours = [field[y_top][x_left], field[y_top][x], field[y_top][x_right],
                  field[y][x_left], field[y][x_right],
                  field[y_bot][x_left], field[y_bot][x], field[y_bot][x_right]]
    return sum(neighbours)
