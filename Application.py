import time

from src import Print
from src.Conway import game_of_life
from src.Game import Game

if __name__ == '__main__':
    iterations = 10000
    lines = 53
    columns = 238

    game = Game(lines, columns)
    game.init_field()

    for i in range(iterations):
        Print.field(game)
        time.sleep(0.016)  # ~ 60hz
        game = game_of_life(game)
